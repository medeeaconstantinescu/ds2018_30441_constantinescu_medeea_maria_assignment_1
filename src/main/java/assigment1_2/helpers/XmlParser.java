package assigment1_2.helpers;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.jdom.*;
import org.jdom.input.SAXBuilder;

public class XmlParser {

	public String parseToGetLocalTime(HttpEntity cityEntity) {

		SAXBuilder builder = new SAXBuilder();
		String localtimeSelectedCity = "";

		try {
			Document documentToParse;

			try {
				documentToParse = builder.build(cityEntity.getContent());
				Element timezoneElement = documentToParse.getRootElement();

				//get the local time from what the api returned, parse the xml
				localtimeSelectedCity = timezoneElement.getChildText("localtime");
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (JDOMException e) {
			System.out.println("Error message from getting local time: " + e.getMessage());
			e.printStackTrace();
		}

		//return the local time as a string
		return localtimeSelectedCity;
	}
}
