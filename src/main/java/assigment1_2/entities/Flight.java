package assigment1_2.entities;

import javax.persistence.*;

@Entity
@Table(name = "flight")
public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private int id;
	
	@Column(name = "flightNumber", nullable = false)
	private String flightNumber;
	
	@Column(name = "airplaneType", nullable = false)
	private String airplaneType;

	@Column(name = "arrivalCity", nullable=true)
	private int arrivalCity;
	
	@Column(name = "departureCity", nullable=true)
	private int departureCity;
	
	@Column(name = "arrivalDate")
	private String arrivalDate;
	
	@Column(name = "departureDate")
	private String departureDate;

	public Flight() {}
	
	public Flight(String flightNumber, String airplaneType, int arrivalCity, int departureCity,
			String departureDate, String arrivalDate) {
		super();
		this.flightNumber = flightNumber;
		this.airplaneType = airplaneType;
		this.arrivalCity = arrivalCity;
		this.departureCity = departureCity;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public int getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(int arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public int getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(int departureCity) {
		this.departureCity = departureCity;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@Override
	public String toString() {
		return "Flight [id=" + id + ", flightNumber=" + flightNumber + ", airplaneType=" + airplaneType
				+ ", arrivalCity=" + arrivalCity + ", departureCity=" + departureCity + ", departureDate="
				+ departureDate + ", arrivalDate=" + arrivalDate + "]";
	}
}