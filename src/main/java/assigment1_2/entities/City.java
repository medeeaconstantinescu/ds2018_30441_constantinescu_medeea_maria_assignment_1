package assigment1_2.entities;

import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "city")
public class City {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private int id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "latitude", nullable = false)
	private String latitude;

	@Column(name = "longitude", nullable = false)
	private String longitude;

	@OneToMany(mappedBy = "arrivalCity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Flight> flights = new HashSet<Flight>();

	public City() {}

	public City(int id, String latitude, String longitude, String name) {
		super();
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getLatitude() {
		return longitude;
	}

	public String getLongitude() {
		return longitude;
	}
}