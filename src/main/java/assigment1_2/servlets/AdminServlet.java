package assigment1_2.servlets;

import javax.servlet.http.*;
import org.hibernate.cfg.Configuration;
import assigment1_2.dao.CityDao;
import assigment1_2.dao.FlightDao;
import assigment1_2.entities.Flight;
import java.io.*;
import java.util.List;
import javax.servlet.ServletException;

/**
 * Servlet implementation class AdminServlet
 */
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private CityDao cityDao;
	private FlightDao flightDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession httpSession = request.getSession(false);

		if (httpSession != null) {
			String userRole = (String) httpSession.getAttribute("role");
			//check the role from the session and if it is not "administrator" redirect the user to the login page
			if (!userRole.equals("administrator")) {
				response.sendRedirect("LoginServlet");
			}
		}

		PrintWriter outPrinter = response.getWriter();
        outPrinter.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n " + "<html>\n"
				+ "<head>" + "<title>" + "Available flights" + "</title></head>\n" + "<body bgcolor = \"#e6e6fa\">\n"
				+ "<h1 align = \"center\">" + "Available flights" + "</h1>\n" + "<br><br>" + "<div>"
				+ "<form method=\"get\" action=\"FlightServlet\">" + "<input type=\"submit\" value = \"New Flight\"/>"
				+ "</form>" + "<div> " + "</br>" + "<div>" + "<table> " + "<tr>" + "<th>Flight No</th>"
				+ " <th>Airplane Type</th> " + "<th>Departure city</th> " + "<th>Arrival city</th> "
				+ "<th>Departure date</th> " + "<th>Arrival date</th> " + "<th> </th> " + "<th> </th> " + "</tr>");

		List<Flight> flightList = flightDao.getFlights();

		for (Flight flight : flightList) {
			String departureCityName = cityDao.getCityBiId(flight.getDepartureCity()).getName();
			String arrivalCityName = cityDao.getCityBiId(flight.getArrivalCity()).getName();
			String departureDate = flight.getDepartureDate();
			String arrivalDate = flight.getArrivalDate();
			String flightNumber = flight.getFlightNumber();
			String airplaneType = flight.getAirplaneType();
			int flightId = flight.getId();

            outPrinter.println("<tr>");
            outPrinter.println("<td>" + flightNumber + "</td>");
            outPrinter.println("<td>" + airplaneType + "</td>");
            outPrinter.println("<td>" + departureCityName + "</td>");
            outPrinter.println("<td>" + arrivalCityName + "</td>");
            outPrinter.println("<td>" + departureDate + "</td>");
            outPrinter.println("<td>" + arrivalDate + "</td>");
            outPrinter.println("<td> <a class=\"button\" href=\"UpdateFlightServlet?id="+flightId+"\"> Update </a> </td>");
            outPrinter.println("<td> <form method=\"post\" action=\"AdminServlet?id=" + flightId
					+ "\"> <input type=\"submit\" class=\"button\" value=\"Delete\">\r\n" + "</form></td>");
            outPrinter.println("</tr>");
		}

        outPrinter.println("</table> </div>");
		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		flightDao.deleteFlight(Integer.parseInt(request.getParameter("id")));
		//after the delete is performed, return to the page where the flights are displayed
		doGet(request, response);
	}

}
