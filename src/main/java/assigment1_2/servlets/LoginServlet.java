package assigment1_2.servlets;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import assigment1_2.dao.UserDao;
import assigment1_2.entities.User;
import org.hibernate.cfg.Configuration;
import java.io.*;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		userDao = new UserDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	//show the login form
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter outPrinter = response.getWriter();

		outPrinter.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n " + "<html>\n"
				+ "<head>" + "<title>" + "Available flights" + "</title></head>\n" + "<body bgcolor = \"#e6e6fa\">\n"
				+ "<h1 align = \"center\">" + "Available flights" + "</h1>\n" + "</br></br>" + "<h2> Login </h2>"
				+ "</br>" + "<form method=\"post\" action=\"LoginServlet\">"
				+ "Username:  <input required=\"required\" type=\"text\" name=\"username\"/></br></br>"
				+ "Password:  <input required=\"required\" type=\"password\" name=\"password\"/></br></br>"
				+ "<input type=\"submit\" class=\"button\" value=\"Login\">\r\n </form>");
		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter outPrinter = response.getWriter();

		//get the username and password from the request
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = userDao.getUserByUsernamePassword(username, password);
		//get the user from db with the given username and password from the request
		//redirect the user depending on its role
		if (user != null) {
			String userRole = user.getRole();
			 if (userRole.equals("customer")) {
				HttpSession session = request.getSession();
				session.setAttribute("role", userRole);
				response.sendRedirect("UserServlet");
			}
			else if (userRole.equals("administrator")) {
				HttpSession session=request.getSession();
				session.setAttribute("role",userRole);
				response.sendRedirect("AdminServlet");
			}
		} else {
			outPrinter.println("<p>Cannot connect. Invalid username or password.</p>");
			request.getRequestDispatcher("LoginServlet").include(request, response);
		}
		outPrinter.close();
	}
}
