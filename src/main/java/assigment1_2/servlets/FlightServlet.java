package assigment1_2.servlets;

import java.io.*;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.hibernate.cfg.Configuration;
import assigment1_2.dao.CityDao;
import assigment1_2.dao.FlightDao;
import assigment1_2.entities.City;
import assigment1_2.entities.Flight;

/**
 * Servlet implementation class AddFlightServlet
 */
public class FlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private CityDao cityDao;
	private FlightDao flightDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FlightServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	//returns the view for adding a new flight
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter outPrinter = response.getWriter();
        outPrinter.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n " + "<html>\n"
				+ "<head>" + "<title>" + "Add new flight" + "</title></head>\n" + "<body bgcolor =\"#e6e6fa\">\n"
				+ "<h1 align=\"center\">" + "Add new flight" + "</h1>\n"
				+ "<form method=\"post\" action=\"FlightServlet\">\r\n"
				+ "Airplane type: <input required=\"required\" type=\"text\" name=\"airplaneType\">\r\n <br><br>"
				+ "Flight No: <input required=\"required\" type=\"text\" name=\"flightNumber\">\r\n <br><br>"
				+ "Departure date: <input required=\"required\" type=\"datetime-local\" name=\"departureDate\">\r\n <br><br>"
				+ "Arrival date: <input required=\"required\" type=\"datetime-local\" name=\"arrivalDate\">\r\n <br><br>"
		);

        outPrinter.println("Departure city: <select name=\"departureCitySelect\"> ");
		List<City> cityList = cityDao.getCities();
		for(City departureCity: cityList) {
            outPrinter.println("<option>" + departureCity.getName() + "</option>");
		}

        outPrinter.println("</select></br></br>Arrival city: <select name=\"arrivalCitySelect\"> ");
		for(City arrivalCity: cityList) {
            outPrinter.println("<option>" + arrivalCity.getName() + "</option>");
		}

        outPrinter.println("</select></br></br><input type=\"submit\" value=\"Add flight\">\r\n</form>");
		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Flight flightToAdd = new Flight();
		String selectedArrivalCity = request.getParameter("arrivalCitySelect");
		String selectedDepartureCity = request.getParameter("departureCitySelect");
		flightToAdd.setDepartureCity(cityDao.getCityByName(selectedDepartureCity).getId());
		flightToAdd.setArrivalCity(cityDao.getCityByName(selectedArrivalCity).getId());

		flightToAdd.setArrivalDate(request.getParameter("arrivalDate"));
		flightToAdd.setFlightNumber(request.getParameter("flightNumber"));
		flightToAdd.setDepartureDate(request.getParameter("departureDate"));
		flightToAdd.setAirplaneType(request.getParameter("airplaneType"));

		//add the new flight
		flightDao.addFlight(flightToAdd);
		response.sendRedirect("/assigment1_2/AdminServlet");
	}
	
	/**
	 * @see HttpServlet#doPut(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	//update a flight
	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter outPrinter = response.getWriter();
		Flight flightToUpdate = flightDao.getFlightById(Integer.parseInt(request.getParameter("id")));
		String flightNumber = flightToUpdate.getFlightNumber();
		int flightId = flightToUpdate.getId();

        outPrinter.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n " + "<html>\n"
				+ "<head>" + "<title>" + "Update flight" + "</title></head>\n" + "<body bgcolor =\"#e6e6fa\">\n"
				+ "<h1 align=\"center\">" + "Update flight with id: " + flightId + "</h1>\n"
				+ "<form method=\"put\" action=\"FlightServlet?id=\"" + request.getParameter("id") + " \">\r\n"
				+ "Flight Number: <input required=\"required\" type=\"text\" name=\"flightNumber\" value=\" "+ flightNumber + ">\r\n <br><br>"
				+ "Airplane Type: <input required=\"required\" type=\"text\" name=\"airplaneType\">\r\n <br><br>"
				+ "Departure City: <input required=\"required\" type=\"text\" name=\"departureCity\">\r\n <br><br>"
				+ "Arrival City: <input required=\"required\" type=\"text\" name=\"arrivalCity\">\r\n <br><br>"
				+ "Departure Date: <input required=\"required\" type=\"date\" name=\"departureDate\">\r\n <br><br>"
				+ "Arrival Date: <input required=\"required\" id=\"date\" type=\"datetime-local\" name=\"arrivalDate\">\r\n <br><br>"
				+ "<br><br><br>"
				+ "<input type=\"submit\" value=\"Add flight\">\r\n"
				+ "</form>"
		);

		response.getWriter().append("</body></html>");
	}
}
