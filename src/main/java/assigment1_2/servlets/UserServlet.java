package assigment1_2.servlets;
import assigment1_2.dao.CityDao;
import assigment1_2.dao.FlightDao;
import assigment1_2.entities.City;
import assigment1_2.entities.Flight;
import assigment1_2.helpers.XmlParser;

import java.io.*;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.hibernate.cfg.Configuration;

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CityDao cityDao;
	private FlightDao flightDao;
	XmlParser xmlParser;

	public UserServlet() {
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
		xmlParser = new XmlParser();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession httpSession = request.getSession(false);

		if (httpSession != null) {
			response.setContentType("text/html");
			PrintWriter outPrinter = response.getWriter();

			outPrinter.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n " + "<html>\n"
					+ "<head>" + "<title>" + "Available flights" + "</title></head>\n" + "<body bgcolor = \"#e6e6fa\">\n"
					+ "<h1 align = \"center\">" + "Available flights" + "</h1>\n"
					+ "<table> <tr><th>Flight number</th> <th>Airplane type</th> <th>Departure city</th> <th>Arrival city</th> <th>Departure date</th> <th>Arrival date</th> "
					+ "</tr>");

			List<Flight> flightList = flightDao.getFlights();
			for (Flight flight : flightList) {
				String departureCityName = cityDao.getCityBiId(flight.getDepartureCity()).getName();
				String arrivalCityName = cityDao.getCityBiId(flight.getArrivalCity()).getName();
				String departureDate = flight.getDepartureDate();
				String arrivalDate = flight.getArrivalDate();
				String flightNumber = flight.getFlightNumber();
				String airplaneType = flight.getAirplaneType();

				outPrinter.println("<tr>");
				outPrinter.println("<td>" + flightNumber + "</td>");
				outPrinter.println("<td>" + airplaneType+ "</td>");
				outPrinter.println("<td>" + departureCityName + "</td>");
				outPrinter.println("<td>" + arrivalCityName + "</td>");
				outPrinter.println("<td>" + departureDate + "</td>");
				outPrinter.println("<td>" + arrivalDate + "</td>");
				outPrinter.println("</tr>");
			}

			outPrinter.println("</table>");
			outPrinter.println("<form method=\"get\" action=\"UserServlet\"></br></br> Local time of city: </br></br>");
			outPrinter.println("Departure city: <select name=\"citySelect\"> ");

			List<City> cityList = cityDao.getCities();
			for (City departureCity : cityList) {
				String departureCityName = departureCity.getName();
				outPrinter.println("<option>" + departureCityName + "</option>");
			}

			outPrinter.println("</select></br></br><input type=\"submit\" value=\"Get localtime for the selected city\">\r\n</form>");

			@SuppressWarnings("deprecation")
			HttpClient defaultHttpClient = new DefaultHttpClient();

			try {
				HttpHost hostTarget = new HttpHost("new.earthtools.org", 80, "http");

				String selectedCityName = request.getParameter("citySelect");
				City city = cityDao.getCityByName(selectedCityName);
				String latitude = city.getLatitude();
				String longitude = city.getLongitude();

				String requestString = "/timezone/" + latitude + "/" + longitude;
				HttpGet httpReequest = new HttpGet(requestString);

				HttpResponse httpResponse = defaultHttpClient.execute(hostTarget, httpReequest);
				HttpEntity responseEntity = httpResponse.getEntity();

				outPrinter.println("<input type=\"text\" value=\"" + xmlParser.parseToGetLocalTime(responseEntity) + "\"/>");
			} catch (Exception e) {
				e.printStackTrace();
			}

			response.getWriter().append("</body></html>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
