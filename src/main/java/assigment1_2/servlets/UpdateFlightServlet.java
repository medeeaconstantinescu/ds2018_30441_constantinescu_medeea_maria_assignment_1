package assigment1_2.servlets;
import assigment1_2.dao.CityDao;
import assigment1_2.dao.FlightDao;
import assigment1_2.entities.Flight;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class UpdateFlightServlet
 */
public class UpdateFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private CityDao cityDao;
	private FlightDao flightDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateFlightServlet() {
		super();
		flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
		cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession httpSession = request.getSession(false);

		if (httpSession != null) {
			String userRole = (String) httpSession.getAttribute("role");
			if (!userRole.equals("administrator")) {
				response.sendRedirect("LoginServlet");
			}
		}

		PrintWriter outPrinter = response.getWriter();

		//get the flight by id
		Flight flight=flightDao.getFlightById(Integer.parseInt(request.getParameter("id")));

		//flight attributes
		String departureCityName = cityDao.getCityBiId(flight.getDepartureCity()).getName();
		String arrivalCityName = cityDao.getCityBiId(flight.getArrivalCity()).getName();
		String departureDate = flight.getDepartureDate();
		String arrivalDate = flight.getArrivalDate();
		String flightNumber = flight.getFlightNumber();
		String airplaneType = flight.getAirplaneType();
		int flightId = flight.getId();

        outPrinter.println("<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n " + "<html>\n"
				+ "<head>" + "<title>" + "Update flight" + "</title></head>\n" + "<body bgcolor =\"#e6e6fa\">\n"
				+ "<h1 align=\"center\">" + "Update flight" + "</h1>\n" + "<form method=\"post\" action=\"UpdateFlightServlet?id="
				+  flightId + " \">\r\n"
				+ "Flight Number: <input required=\"required\" type=\"text\" name=\"flightNumber\" value=\" "
				+  flightNumber + "\">\r\n <br><br>"
				+ "Airplane Type: <input required=\"required\" type=\"text\" name=\"airplaneType\" value=\" "
				+ airplaneType + "\">\r\n <br><br>"
				+ "Departure City: <input required=\"required\" type=\"text\" name=\"departureCity\" value=\" "
				+ departureCityName + "\"readonly >\r\n <br><br>"
				+ "Arrival City: <input type=\"text\" name=\"arrivalCity\" value=\" "
				+ arrivalCityName + "\" readonly >\r\n <br><br>"
				+ "Departure Date: <input required=\"required\" type=\"datetime-local\" name=\"departureDate\" value=\" "
				+ departureDate + "\">\r\n <br><br>"
				+ "Arrival Date: <input required=\"required\" id=\"date\" type=\"datetime-local\" name=\"arrivalDate\" value=\" "
				+ arrivalDate + "\">\r\n <br><br>"
				+ "<input type=\"submit\" value=\"Update flight\">\r\n" + "</form>"
		);

		response.getWriter().append("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	    //get the flight by id
		Flight flightToUpdate = flightDao.getFlightById(Integer.parseInt(request.getParameter("id")));

		//get flight attributes from request
		String flightNumber = request.getParameter("flightNumber");
        String airplaneType = request.getParameter("airplaneType");
        String departureDate = request.getParameter("departureDate");
        String arrivalDate = request.getParameter("arrivalDate");

        //set the new attributes to the flight found
        flightToUpdate.setDepartureDate(departureDate);
        flightToUpdate.setArrivalDate(arrivalDate);
		flightToUpdate.setFlightNumber(flightNumber);
		flightToUpdate.setAirplaneType(airplaneType);

		flightDao.editFlight(flightToUpdate);

		//after the update is made, redirect the administrator to the page where all the flights are displayed
		response.sendRedirect("/assigment1_2/AdminServlet");
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

}
