package assigment1_2.dao;

import java.util.List;
import org.hibernate.*;
import assigment1_2.entities.Flight;

public class FlightDao {

	private SessionFactory sessionFactory;

	public FlightDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Flight addFlight(Flight flight) {
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;

		//try to add a new flight
		try {
			transaction = currentSession.beginTransaction();
			currentSession.save(flight);
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Error message from add flight: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//return the added flight
		return flight;
	}
	
	public Flight editFlight(Flight flightToUpdate) {
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;

		//try to edit the flight
		try {
			transaction = currentSession.beginTransaction();
			currentSession.update(flightToUpdate);
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Error message from update flight: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//return the updated flight
		return flightToUpdate;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> getFlights() {
		List<Flight> flightList = null;
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;

		//try to get all the flights
		try {
			transaction = currentSession.beginTransaction();
			flightList = currentSession.createQuery("FROM Flight").list();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Message from getting all flights: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//returns a list of flights that exist in the Flight table in the database
		return flightList;
	}

	@SuppressWarnings("unchecked")
	public Flight getFlightById(int flightId) {
		List<Flight> flightList = null;
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;

		//try to get the flight by the given id
		try {
			transaction = currentSession.beginTransaction();
			Query query = currentSession.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", flightId);
			flightList = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Error message from getting flight by id: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//returns a list of flights having the given id, but only the first element is taken
		return flightList != null && !flightList.isEmpty() ? flightList.get(0) : null;
	}

	public void deleteFlight(int flightId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("DELETE FROM Flight WHERE id =" + flightId);
			query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
	}
}
