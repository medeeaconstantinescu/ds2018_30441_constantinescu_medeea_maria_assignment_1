package assigment1_2.dao;

import java.util.List;
import org.hibernate.*;
import assigment1_2.entities.City;

public class CityDao {
	
	private SessionFactory sessionFactory;

	public CityDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public List<City> getCities() {
		List<City> cities = null;
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;
		
		try {
			transaction = currentSession.beginTransaction();
			cities = currentSession.createQuery("FROM City").list();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Message from getting all cities: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//return all the cities from the table City
		return cities;
	}

	@SuppressWarnings("unchecked")
	public City getCityBiId(int cityId) {
		List<City> cityList = null;
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;

		//try to get the city by its id
		try {
			transaction = currentSession.beginTransaction();
			Query query = currentSession.createQuery("FROM City WHERE id = :id");
			query.setParameter("id", cityId);
			cityList = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Message from getting city by id: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//the query returns the cities with that id but I will take the first one if the list is not null
		return cityList != null && !cityList.isEmpty() ? cityList.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public City getCityByName(String cityName) {
		List<City> cityList = null;
		Session currentSession = sessionFactory.openSession();
		Transaction transaction = null;

		//try to get the city by its name
		try {
			transaction = currentSession.beginTransaction();
			Query query = currentSession.createQuery("FROM City WHERE name = :name");
			query.setParameter("name", cityName);
			cityList = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Message from getting the city by its name: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//the query returns a list of cities with that name but I will take the first one if the list is not null
		return cityList != null && !cityList.isEmpty() ? cityList.get(0) : null;
	}
}
