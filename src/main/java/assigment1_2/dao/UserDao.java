package assigment1_2.dao;

import java.util.List;

import org.hibernate.*;

import assigment1_2.entities.User;

public class UserDao {
	private SessionFactory factory;

	public UserDao(SessionFactory factory) {
		this.factory = factory;
	}

	@SuppressWarnings("unchecked")
	public User getUserByUsernamePassword(String username, String password) {
		List<User> userList = null;
		Session currentSession = factory.openSession();
		Transaction transaction = null;

		//try to get the user having the username and password given from User table
		try {
			transaction = currentSession.beginTransaction();
			Query query = currentSession.createQuery("FROM User WHERE username = :username AND password = :password");
			query.setParameter("username", username);
			query.setParameter("password", password);
			userList = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			System.out.println("Error message from getting a user by username and password: " + e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			currentSession.close();
		}

		//return the user if it was found
		return userList != null && !userList.isEmpty() ? userList.get(0) : null;
	}
}
